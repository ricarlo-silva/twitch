package br.com.bancopan.twitch.db

import android.support.test.runner.AndroidJUnit4
import br.com.bancopan.twitch.utils.LiveDataTestUtil
import br.com.bancopan.twitch.vo.Game
import org.hamcrest.CoreMatchers
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.*


/**
 * Created by ricarlosilva on 20/12/2017.
 */


@RunWith(AndroidJUnit4::class)
class GameDaoTest : DbTest() {

    @Test
    @Throws(InterruptedException::class)
    fun insertAndRead() {
        val id = 11111
        val name = "Call of Duty: WWII"
        val game = Game(id = id, name = name, extraChannels = 22222, extraViewers = 3333)
        db.game().insert(listOf(game))

        val loaded = LiveDataTestUtil.getValue(db.game().findByName(name))
        assertThat(loaded, CoreMatchers.notNullValue())
        assertThat(loaded.name, CoreMatchers.`is`(name))
        assertThat(loaded.id, CoreMatchers.`is`(id))

    }

}
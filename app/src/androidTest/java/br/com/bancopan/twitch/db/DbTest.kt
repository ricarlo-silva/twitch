package br.com.bancopan.twitch.db

import android.support.test.InstrumentationRegistry
import org.junit.After
import org.junit.Before

/**
 * Created by ricarlosilva on 20/12/2017.
 */

abstract class DbTest {
    protected lateinit var db: TwitchDb

    @Before
    fun initDb() {

        db = TwitchDb.create(InstrumentationRegistry.getContext(), true)
    }

    @After
    fun closeDb() {
        db.close()
    }
}

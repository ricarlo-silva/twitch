package br.com.bancopan.twitch.utils.extensions

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

/**
 * Created by ricarlosilva on 19/12/2017.
 */

fun Any.toJson(prettyPrinting: Boolean = false): String {
    val builder = GsonBuilder()

    if (prettyPrinting) {
        builder.setPrettyPrinting()
    }

    return builder.create().toJson(this)

}

inline fun <reified T> Any.fromJson(json: String?): T {
    val type = object : TypeToken<T>() {}.type
    return Gson().fromJson(json, type)
}
package br.com.bancopan.twitch.utils.extensions

import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

/**
 * Created by ricarlosilva on 19/12/2017.
 */


fun AppCompatActivity.toast(message: String?, time: Int = Toast.LENGTH_SHORT) {
    message?.let {
        Toast.makeText(this, message, time).show()
    }
}

fun AppCompatActivity.snackbar(message: String?, time: Int = Snackbar.LENGTH_SHORT) {
    message?.let {
        this.currentFocus
        Snackbar.make(currentFocus, message, time).show()
    }
}
package br.com.bancopan.twitch.db.converter

import android.arch.persistence.room.TypeConverter
import br.com.bancopan.twitch.utils.extensions.fromJson
import br.com.bancopan.twitch.utils.extensions.toJson
import br.com.bancopan.twitch.vo.Box

/**
 * Created by ricarlosilva on 19/12/2017.
 */
class BoxConverter {

    @TypeConverter
    fun fromJson(value: String?): Box? = fromJson<Box>(value)

    @TypeConverter
    fun toJson(value: Box?): String? = value?.toJson()
}
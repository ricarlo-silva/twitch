package br.com.bancopan.twitch.db.converter

import android.arch.persistence.room.TypeConverter
import br.com.bancopan.twitch.utils.extensions.fromJson
import br.com.bancopan.twitch.utils.extensions.toJson
import br.com.bancopan.twitch.vo.Logo

/**
 * Created by ricarlosilva on 19/12/2017.
 */
class LogoConverter {

    @TypeConverter
    fun fromJson(value: String?): Logo? = fromJson<Logo>(value)

    @TypeConverter
    fun toJson(value: Logo?): String? = value?.toJson()
}
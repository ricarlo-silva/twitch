package br.com.bancopan.twitch.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by ricarlosilva on 19/12/2017.
 */
object AndroidUtils {

    fun isNetworkAvaliable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}
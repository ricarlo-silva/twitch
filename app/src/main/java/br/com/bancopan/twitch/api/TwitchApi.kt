package br.com.bancopan.twitch.api

import br.com.bancopan.twitch.Constants
import br.com.bancopan.twitch.vo.ApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by ricarlosilva on 19/12/2017.
 */
interface TwitchApi {

    @GET("games/top")
    fun getTop(@Query("offset") offset: Int,
               @Query("limit") limit: Int,
               @Query("client_id") client_id: String = Constants.clientId) : Call<ApiResponse>
}
package br.com.bancopan.twitch.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import br.com.bancopan.twitch.vo.Game


/**
 * Created by ricarlosilva on 19/12/2017.
 */
@Dao
interface TwitchGameDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(games : List<Game>)

    @Query("SELECT * FROM games ORDER BY name ASC")
    fun findAll(): LiveData<List<Game>>

    @Query("SELECT * FROM games WHERE name = :name")
    fun findByName(name: String): LiveData<Game>

}
package br.com.bancopan.twitch.vo


import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName


data class TopGame(

        @field:SerializedName("game")
        val game: Game,

        @field:SerializedName("viewers")
        val viewers: Int? = null,

        @field:SerializedName("channels")
        val channels: Int? = null
)
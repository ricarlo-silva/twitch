package br.com.bancopan.twitch.repository

/**
 * Created by ricarlosilva on 19/12/2017.
 */

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}
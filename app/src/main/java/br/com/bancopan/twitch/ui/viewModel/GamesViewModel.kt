package br.com.bancopan.twitch.ui.viewModel

import android.arch.lifecycle.*
import br.com.bancopan.twitch.repository.GameRepository
import br.com.bancopan.twitch.repository.Resource
import br.com.bancopan.twitch.vo.Game

/**
 * Created by ricarlosilva on 19/12/2017.
 */
class GamesViewModel(private val repository: GameRepository) : ViewModel() {

    private var result: LiveData<Resource<List<Game>>> = MediatorLiveData()
    private val page = MediatorLiveData<Int>()

    init {
        page.value = 0
        result = Transformations.switchMap(page) { page -> repository.getGames(page) }

    }

    fun getGames() = result

    fun setPage(page: Int) {
        this.page.value = page
    }

}
package br.com.bancopan.twitch.ui.adapter

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView

/**
 * Created by ricarlosilva on 20/12/2017.
 */
abstract class GridEndlessRecyclerViewScrollListener(private val gridLayoutManager: GridLayoutManager) : RecyclerView.OnScrollListener() {
    private var previousItemCount: Int = 0
    private var loading: Boolean = false
    private val startingPageIndex = 0
    private var offset = startingPageIndex
    private val limit = 10

    init {
        this.reset()
    }

    override fun onScrolled(view: RecyclerView?, dx: Int, dy: Int) {
        if (dy > 0) {
            val itemCount = gridLayoutManager.itemCount
            val lastVisible = gridLayoutManager.findLastVisibleItemPosition()

            if (!loading && lastVisible >= itemCount - 1) {
                previousItemCount = itemCount
                loading = true
                loadMoreItems(offset, limit)
                offset = if(offset == startingPageIndex) limit else offset + limit
            }
        }
    }

    fun setLoading(loading: Boolean) {
        this.loading = loading
    }

    fun reset() {
        this.loading = false
        this.offset = startingPageIndex
        this.previousItemCount = -1
    }

    protected abstract fun loadMoreItems(offset: Int, limit: Int)
}

package br.com.bancopan.twitch.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import br.com.bancopan.twitch.R
import br.com.bancopan.twitch.vo.Game
import com.bumptech.glide.RequestManager

/**
 * Created by ricarlosilva on 20/12/2017.
 */
class GameAdapter(private val glide: RequestManager, private val onItemClicked: OnItemClicked)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnItemClicked {
        fun onGameClick(item: Game, imageView: ImageView)
    }

    private var items: MutableList<Game> = mutableListOf()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_game -> (holder as GameViewHolder).bind(getItem(position))
        //R.layout.network_state_item -> (holder as NetworkStateItemViewHolder).bindTo( networkState)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        onBindViewHolder(holder, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_game -> GameViewHolder.create(parent, glide, onItemClicked)
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return  R.layout.item_game
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun getItem(position: Int) = items[position]

    private fun add(item: Game){
        items.add(item)
        notifyItemInserted(items.size - 1)
    }

    fun addAll(list : List<Game>?){
        list?.forEach {
            if(!items.contains(it)){
                add(it)
            }
        }
    }

    private fun remove(item: Game) {
        val position = items.indexOf(item)
        if (position > -1) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }

}
package br.com.bancopan.twitch.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import br.com.bancopan.twitch.db.converter.BoxConverter
import br.com.bancopan.twitch.db.converter.LogoConverter
import br.com.bancopan.twitch.vo.Game

/**
 * Created by ricarlosilva on 19/12/2017.
 */

@Database(
        entities = [(Game::class)],
        version = 1,
        exportSchema = false
)
@TypeConverters(
        BoxConverter::class,
        LogoConverter::class
)
abstract class TwitchDb : RoomDatabase() {
    companion object {
        fun create(context: Context, useInMemory : Boolean = false): TwitchDb {
            val databaseBuilder = if(useInMemory) {
                Room.inMemoryDatabaseBuilder(context, TwitchDb::class.java)
            } else {
                Room.databaseBuilder(context, TwitchDb::class.java, "twitch.db")
            }
            return databaseBuilder
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }

    abstract fun game(): TwitchGameDao
}
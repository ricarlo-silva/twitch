package br.com.bancopan.twitch.vo


import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Entity(tableName = "games")
data class Game(

        @PrimaryKey
        @field:SerializedName("_id")
        var id: Int = 0,

        @Ignore
        @field:SerializedName("_links")
        var links: Links? = null,

        @field:SerializedName("giantbomb_id")
        var giantbombId: Int? = null,

        @field:SerializedName("popularity")
        var popularity: Int? = null,

        @field:SerializedName("name")
        var name: String? = null,

        @field:SerializedName("logo")
        var logo: Logo? = null,

        @field:SerializedName("box")
        var box: Box? = null,

        @field:SerializedName("locale")
        var locale: String? = null,

        @field:SerializedName("localized_name")
        var localizedName: String? = null,

        @field:SerializedName("extra_viewers")
        var extraViewers: Int = 0,

        @field:SerializedName("extra_channels")
        var extraChannels: Int = 0


) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readParcelable<Links>(Links::class.java.classLoader),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readParcelable<Logo>(Logo::class.java.classLoader),
            source.readParcelable<Box>(Box::class.java.classLoader),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeParcelable(links, 0)
        writeValue(giantbombId)
        writeValue(popularity)
        writeString(name)
        writeParcelable(logo, 0)
        writeParcelable(box, 0)
        writeString(locale)
        writeString(localizedName)
        writeInt(extraViewers)
        writeInt(extraChannels)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Game> = object : Parcelable.Creator<Game> {
            override fun createFromParcel(source: Parcel): Game = Game(source)
            override fun newArray(size: Int): Array<Game?> = arrayOfNulls(size)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Game

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int = id.hashCode()
}
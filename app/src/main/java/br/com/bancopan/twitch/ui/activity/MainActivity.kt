package br.com.bancopan.twitch.ui.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.ImageView
import br.com.bancopan.twitch.R
import br.com.bancopan.twitch.repository.GameRepository
import br.com.bancopan.twitch.repository.Status
import br.com.bancopan.twitch.ui.adapter.GameAdapter
import br.com.bancopan.twitch.ui.adapter.GridEndlessRecyclerViewScrollListener
import br.com.bancopan.twitch.ui.viewModel.GamesViewModel
import br.com.bancopan.twitch.utils.extensions.snackbar
import br.com.bancopan.twitch.vo.Game
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), GameAdapter.OnItemClicked {

    private lateinit var adapter: GameAdapter
    private lateinit var viewModel: GamesViewModel
    private var numColumns = 2
    private lateinit var scrollListener: GridEndlessRecyclerViewScrollListener


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()
        viewModel = getViewModel()
        subscribeUi(viewModel)
    }

    private fun initRecyclerView() {

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            adapter.clear()
            viewModel.setPage(0)
            scrollListener.reset()

        }

        numColumns = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 3 else 2

        val glide = Glide.with(this)
        adapter = GameAdapter(glide, this)
        val layoutManager = GridLayoutManager(this, numColumns)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter


        scrollListener = object : GridEndlessRecyclerViewScrollListener(layoutManager) {
            override fun loadMoreItems(offset: Int, limit: Int) {
                viewModel.setPage(offset)
                progressBar.visibility = View.VISIBLE
            }
        }

        recyclerView.addOnScrollListener(scrollListener)

    }

    private fun getViewModel(): GamesViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return GamesViewModel(GameRepository.getInstance()) as T
            }
        })[GamesViewModel::class.java]
    }

    private fun subscribeUi(viewModel: GamesViewModel) {

        viewModel.getGames().observe(this, Observer {
            it?.let {
                when (it.status) {
                    Status.RUNNING -> {
                        scrollListener.setLoading(true)

                        progressBar.visibility = if (swipeRefresh.isRefreshing) View.GONE else View.VISIBLE

                    }
                    Status.FAILED -> {
                        swipeRefresh.isRefreshing = false
                        snackbar(it.message)
                        scrollListener.setLoading(swipeRefresh.isRefreshing)

                        progressBar.visibility = View.GONE

                    }
                    Status.SUCCESS -> {
                        swipeRefresh.isRefreshing = false
                        snackbar(it.message)
                        scrollListener.setLoading(swipeRefresh.isRefreshing)

                        progressBar.visibility = View.GONE
                        adapter.addAll(it.data)

                    }

                }
            }

        })

    }

    override fun onGameClick(item: Game, ivPhoto: ImageView) {

        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.EXTRA_GAME, item)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, ivPhoto, ViewCompat.getTransitionName(ivPhoto))
            startActivity(intent, options.toBundle())

        } else {
            startActivity(intent)
        }

    }
}

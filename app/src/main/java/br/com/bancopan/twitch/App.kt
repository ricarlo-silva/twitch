package br.com.bancopan.twitch

import android.app.Application

/**
 * Created by ricarlosilva on 19/12/2017.
 */
class App : Application() {

    private lateinit var mAppExecutors: AppExecutors

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        mAppExecutors = AppExecutors()
    }

    fun getAppExecutors() = mAppExecutors

    companion object {
        private lateinit var appInstance: App

        fun getInstance() = appInstance
    }
}
package br.com.bancopan.twitch.vo

import com.google.gson.annotations.SerializedName

data class ApiResponse(

        @field:SerializedName("top")
        val top: List<TopGame>,

        @field:SerializedName("_links")
        val links: Links? = null,

        @field:SerializedName("_total")
        val total: Int
)
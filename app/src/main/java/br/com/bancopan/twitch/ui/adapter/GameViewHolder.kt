package br.com.bancopan.twitch.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.bancopan.twitch.R
import br.com.bancopan.twitch.vo.Game
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions

/**
 * Created by ricarlosilva on 20/12/2017.
 */

class GameViewHolder(view: View, private val glide: RequestManager, onItemClicked: GameAdapter.OnItemClicked) : RecyclerView.ViewHolder(view) {
    private val tvName: TextView = view.findViewById(R.id.tvName)
    private val ivGame: ImageView = view.findViewById(R.id.ivGame)
    private var game: Game? = null

    init {
        view.setOnClickListener {
            onItemClicked.onGameClick(game!!, ivGame)

        }
    }

    fun bind(game: Game?) {
        this.game = game
        tvName.text = game?.name ?: "loading"

        if (game?.box?.medium != null) {
            ivGame.visibility = View.VISIBLE
            glide.load(game.box!!.medium)
                    .apply(RequestOptions().centerCrop())
                    .into(ivGame)
        } else {
            ivGame.visibility = View.GONE
        }

    }

    companion object {
        fun create(parent: ViewGroup, glide: RequestManager, onItemClicked: GameAdapter.OnItemClicked): GameViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_game, parent, false)
            return GameViewHolder(view, glide, onItemClicked)
        }
    }

}
package br.com.bancopan.twitch.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import br.com.bancopan.twitch.App
import br.com.bancopan.twitch.AppExecutors
import br.com.bancopan.twitch.R
import br.com.bancopan.twitch.api.ServiceGenerator
import br.com.bancopan.twitch.api.TwitchApi
import br.com.bancopan.twitch.db.TwitchDb
import br.com.bancopan.twitch.utils.AndroidUtils
import br.com.bancopan.twitch.vo.Game
import br.com.bancopan.twitch.vo.ApiResponse
import retrofit2.Call
import retrofit2.Callback

/**
 * Created by ricarlosilva on 19/12/2017.
 */
class GameRepository(private val twitchApi: TwitchApi, private val db: TwitchDb, private val appExecutors: AppExecutors) {

    companion object {
        fun getInstance(): GameRepository {
            val api = ServiceGenerator.createService(TwitchApi::class.java)
            val db = TwitchDb.create(App.getInstance())
            val exec = App.getInstance().getAppExecutors()

            return GameRepository(api, db, exec)
        }
    }

    fun getGames(offset: Int, limit: Int = 10): LiveData<Resource<List<Game>>> {

        val liveData = MediatorLiveData<Resource<List<Game>>>()
        liveData.postValue(Resource.loading())

        twitchApi.getTop(offset, limit).enqueue(object : Callback<ApiResponse> {
            override fun onFailure(call: Call<ApiResponse>?, t: Throwable?) {


                if (!AndroidUtils.isNetworkAvaliable(App.getInstance())) {

                    val dbSource = loadFromDb()
                    liveData.addSource(dbSource, {
                        liveData.removeSource(dbSource)
                        liveData.value = Resource.success(it, message = App.getInstance().getString(R.string.no_connected))
                    })
                    //liveData.postValue(Resource.error(App.getInstance().getString(R.string.no_connected)))
                } else {
                    liveData.value = Resource.error(t?.message ?: "error")
                }
            }

            override fun onResponse(call: Call<ApiResponse>?, response: retrofit2.Response<ApiResponse>?) {

                if (response != null && response.isSuccessful) {
                    val list = compatResponse(response.body())
                    insertResultIntoDb(list)
                    liveData.value = Resource.success(list)
                    //liveData.postValue(Resource.success(list))

                } else {
                    liveData.value = Resource.error("error code: ${response?.code()}")

                }
            }
        })

        return liveData
    }


    private fun compatResponse(response: ApiResponse?): List<Game> {
        val list = mutableListOf<Game>()
        response?.top?.map {
            it.game.extraViewers = it.viewers ?: 0
            it.game.extraChannels = it.channels ?: 0

            list.add(it.game)
        }

        return list
    }

    private fun insertResultIntoDb(games: List<Game>) {
        appExecutors.diskIO().execute {
            db.runInTransaction {
                db.game().insert(games)
            }
        }
    }

    private fun loadFromDb() = db.game().findAll()


}
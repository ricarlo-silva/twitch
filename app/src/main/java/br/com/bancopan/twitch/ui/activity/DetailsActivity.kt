package br.com.bancopan.twitch.ui.activity

import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import br.com.bancopan.twitch.R
import br.com.bancopan.twitch.vo.Game
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    companion object {
        val EXTRA_GAME = "extra_game"
    }

    private val game by lazy { intent.getParcelableExtra<Game>(EXTRA_GAME) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        supportPostponeEnterTransition()

        this.setTitle()
        game.box?.medium?.let { path -> loadImage(path, ivPhoto) }
        tvViewers.text = getString(R.string.details_viewers, game.extraViewers)
        tvChannels.text = getString(R.string.details_channels, game.extraChannels)

    }

    private fun setTitle(){
        supportActionBar?.title = game.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun loadImage(path: String, imageView: ImageView) {
        Glide.with(this)
                .load(path)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                        supportStartPostponedEnterTransition()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                        supportStartPostponedEnterTransition()
                        return false
                    }
                })
                .apply(RequestOptions()
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(imageView)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
        // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                supportFinishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

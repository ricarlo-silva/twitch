package br.com.bancopan.twitch.vo

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Links(

        @field:SerializedName("next")
        val next: String? = null,

        @field:SerializedName("self")
        val self: String? = null
) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeString(next)
                writeString(self)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<Links> = object : Parcelable.Creator<Links> {
                        override fun createFromParcel(source: Parcel): Links = Links(source)
                        override fun newArray(size: Int): Array<Links?> = arrayOfNulls(size)
                }
        }
}
package br.com.bancopan.twitch.vo


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Box(

        @field:SerializedName("small")
        val small: String? = null,

        @field:SerializedName("template")
        val template: String? = null,

        @field:SerializedName("large")
        val large: String? = null,

        @field:SerializedName("medium")
        val medium: String? = null
) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeString(small)
                writeString(template)
                writeString(large)
                writeString(medium)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<Box> = object : Parcelable.Creator<Box> {
                        override fun createFromParcel(source: Parcel): Box = Box(source)
                        override fun newArray(size: Int): Array<Box?> = arrayOfNulls(size)
                }
        }
}
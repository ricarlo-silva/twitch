# Avaliação para time Tecnologia Digital #


1.	Desafio PAN Android Mobile:


O objetivo do teste é fazer uma lista em grid com a imagem e o nome do jogo e uma tela de detalhes com a imagem, nome do jogo, contador de canais e quantidade de visualizações.

2.	API:


Para desenvolver o app você vai precisar usar o endpoint de Top Games do Twitch. Mais informações: https://dev.twitch.tv/docs/v5/reference/games/.


### Requisitos Essenciais:
    
    A.	Linguagem: Kotlin
    B.	Paginação na tela de lista, com endless scroll / scroll infinito.
    C.	Tratamento de falha de conexão: Avise o usuário quando o download dos jogos falhar por falta de conexão.
    D.	Pull to refresh.
    E.	Cache das informações dos jogos para acesso offline


### O que foi utilizado? ###

* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
    * Room Persistence Library
    * LiveData
    * ViewModel
* Comunicação com API - [Retrofit](http://square.github.io/retrofit/)
* Load e cache de imagens - [Glide](https://github.com/bumptech/glide)
* Android [Material Design](https://material.io/guidelines/#introduction-principles)

### Exemplos ###

![alt text](captures/capture_01.png "img 1")
![alt text](captures/capture_02.png "img 2")


